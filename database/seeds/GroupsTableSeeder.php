<?php

use App\Group;
use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group = new Group;
        $group->name = 'A';
        $group->description = '7-8 años';
        $group->save();

        $group = new Group;
        $group->name = 'B';
        $group->description = '9-10 años';
        $group->save();

        $group = new Group;
        $group->name = 'C';
        $group->description = '11-12 años';
        $group->save();
    }
}
